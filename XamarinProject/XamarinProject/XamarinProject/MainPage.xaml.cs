﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Globalization;

namespace XamarinProject
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private int _currentPlanet = 0;
        private List<Planets> _planets = new List<Planets>();

        public MainPage()
        {
            InitializeComponent();
        }


        #region Buttons

        private async void Button_Clicked(object sender, EventArgs e) //Обработчик нажатия на кнопку
        {
            Loader.IsRunning = true;

            await GetJson();

            PlanetLabels.Children.Clear();
            btnGetPlanet.IsEnabled = false;
            btnNext.IsEnabled = true;

            ViewPlanet();
            Loader.IsRunning = false;
        }

        private void Button_Previous(object sender, EventArgs e)
        {
            if (_currentPlanet - 1 >= 0)
            {
                _currentPlanet--;
                PlanetLabels.Children.Clear();
                ViewPlanet();
            }
        }

        private void Button_Next(object sender, EventArgs e)
        {
            if (_currentPlanet + 1 < _planets.Count)
            {
                _currentPlanet++;
                PlanetLabels.Children.Clear();
                ViewPlanet();
            }

            btnPrev.IsEnabled = true;
        }

        #endregion


        private void ViewPlanet()
        {
            var properties = _planets[_currentPlanet].GetType().GetProperties();
            foreach (var prop in properties)
            {
                if (prop.GetValue(_planets[_currentPlanet]) is String)
                {
                    var value = prop.GetValue(_planets[_currentPlanet]);
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    PlanetLabels.Children.Add(new Label()
                    {
                        Text = $"{textInfo.ToTitleCase(prop.Name)}: {value}",
                        Style = Device.Styles.ListItemDetailTextStyle,
                        TextColor = Color.Black

                    });
                }
            }
        }

        private async Task GetJson()
        {

            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage
            {
                RequestUri = new Uri("https://swapi.co/api/planets/"),
                Method = HttpMethod.Get
            };
            request.Headers.Add("Accept", "application/json");

            var response = await client.SendAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                HttpContent responseContent = response.Content;
                var json = await responseContent.ReadAsStringAsync();
                JObject planetsObj = JObject.Parse(json);
                _planets = JsonConvert.DeserializeObject<List<Planets>>(planetsObj.GetValue("results").ToString());

            }


        }
    }
}
